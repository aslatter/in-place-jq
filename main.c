#include <errno.h>
#include <fcntl.h>
#include <libgen.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

void parseArgs(int argc, char **argv, char ***jqArgs, char **inFileName);
int execAndWait(char *program, char **args, int inFd, int outFd);
void perrorf(const char *format, ...);

int main(int argc, char **argv) {
  if (argc < 3) {
    fprintf(stderr, "missing required inputs!\n");
    exit(1);
  }

  char **jqArgs;
  char *inFileName;

  // divide up input argv into filename and jq args
  parseArgs(argc, argv, &jqArgs, &inFileName);

  if (inFileName == NULL) {
    fprintf(stderr, "missing input file\n");
    exit(1);
  }

  char *tmpStr1 = strdup(inFileName);
  char *dirName = dirname(tmpStr1);

  char *inBaseName = basename(inFileName);

  int dir = open(dirName, 0, O_RDONLY);
  if (dir < 0) {
    perror("error accessing output directory");
    exit(1);
  }
  free(tmpStr1);
  tmpStr1 = NULL;
  dirName = NULL;

  int inFd = openat(dir, inBaseName, 0, O_RDONLY);
  if (inFd < 0) {
    perror("error opening input file");
    exit(1);
  }

  // get file-mode for new file from old file
  struct stat statbuf;
  if (fstat(inFd, &statbuf) != 0) {
    perror("error on stat of input file");
    exit(1);
  }

  mode_t mode = statbuf.st_mode & 0777;

  // creat a new, unlinked temp-file in the same folder.
  int outFd = openat(dir, ".", O_TMPFILE | O_RDWR, mode);
  if (outFd < 0) {
    perror("error opening output file");
    exit(1);
  }

  // run jq!
  int jqRet = execAndWait("jq", jqArgs, inFd, outFd);
  if (jqRet != 0) {
    // we assume stderr had something useful, and
    // exit without printing any messages.
    exit(jqRet);
  }

  // link the output file over-top of the input file.
  // we made them in the same folder, so hopfully they are
  // in the same file-system.

  // we go through /proc, so let's do an up-front check
  // to see if we think that will even work.
  char fdPath[50];
  snprintf(fdPath, 50, "/proc/self/fd/%d", outFd);

  if (stat(fdPath, &statbuf) != 0) {
    perror("weird error - not sure here");
    exit(1);
  }

  // unlink old file
  if (unlinkat(dir, inBaseName, 0) != 0) {
    perror("error removing input file");
    exit(1);
  }

  // link new file
  if (linkat(AT_FDCWD, fdPath, dir, inBaseName, AT_SYMLINK_FOLLOW) != 0) {
    perror("error replacing input file");
    exit(1);
  }

  close(outFd);
  close(inFd);
  free(jqArgs);
}

void parseArgs(int argc, char **argv, char ***pjqArgs, char **pInFileName) {
  char **jqArgs = malloc(sizeof(char *) * argc + 1);
  jqArgs[0] = "jq";

  char *inFileName = NULL;
  bool doneWithFlags = false;

  int j = 1; // j: position in jqArgs
  for (int i = 1; i < argc; i++) {
    char *str = argv[i];
    if (str == NULL) {
      // ??
      break;
    }
    if (!doneWithFlags && strcmp(str, "--") == 0) {
      doneWithFlags = true;
      // no need to pass this arg on to jq
      continue;
    }
    if (inFileName != NULL) {
      // we've found our input file - just copy the args over
      jqArgs[j++] = str;
      continue;
    }
    if (strlen(str) == 0) {
      // ??
      jqArgs[j++] = str;
      continue;
    }
    if (strcmp(str, "-") == 0) {
      // do not support re-writig stdin in-place
      fprintf(stderr, "re-writing stdin not supported\n");
      exit(1);
    }
    if (!doneWithFlags && str[0] == '-') {
      // looks like a flag, not a file-name
      jqArgs[j++] = str;
      continue;
    }
    // we found our input file-name!
    inFileName = str;
  }
  jqArgs[j] = NULL;

  *pjqArgs = jqArgs;
  *pInFileName = inFileName;
}

// execAndWait inokes a program with the requested stdin, stdout
// and arguments. Zero is returned on success. Some
// errors may result in program termination.
int execAndWait(char *program, char **args, int inFd, int outFd) {
  // fork + exec jq
  pid_t child = fork();
  if (child == 0) {
    // child. set up stdin and stdout
    if (inFd != STDIN_FILENO) {
      dup2(inFd, STDIN_FILENO);
      close(inFd);
    }
    if (outFd != STDOUT_FILENO) {
      dup2(outFd, STDOUT_FILENO);
      close(outFd);
    }
    execvp(program, args);
    // execvp does not return on success
    perrorf("unable to run %s", program);
    exit(1);
  }
  if (child < 0) {
    perrorf("failed to start %s", program);
    exit(1);
  }

  // parent

  // wait for child
  int wstatus;
  while (true) {
    if (waitpid(child, &wstatus, 0) != -1) {
      break;
    }
    if (errno != EINTR) {
      perrorf("error waiting for %s to complete", program);
      exit(1);
    }
    // got interupted, wait again
  }

  if (!WIFEXITED(wstatus)) {
    // abnormal exit
    return 1;
  }

  // normal exit - return status-code
  return WEXITSTATUS(wstatus);
}

// perrorf is like perror but with formating.
void perrorf(const char *format, ...) {
  if (format == NULL || format[0] == '\0') {
    perror(NULL);
    return;
  }
  int old_errno = errno;

  va_list ap;
  va_start(ap, format);
  vfprintf(stderr, format, ap);
  va_end(ap);

  fputs(": ", stderr);

  errno = old_errno;
  perror(NULL);

  return;
}
