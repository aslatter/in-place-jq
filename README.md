# in-place jq

This project is a study of working with files in C/Unix.
It is a small C program which wraps *[jq]* and allows operating
on a file in-place.

Thq *jq* program can be used to manipulate JSON data, and takes input on *stdin*
and produces new JSON data on *stdout*. This wrapper allows specifying a file
to modify as an argument.

# Usage

```
in_place_jq <file to modify> [jq arguments]
```

The file-to-modify is the first non-flag argument found. If
you wish to modify a file whose name begins with a "-" you may
use "--" to denote the end of flag-arguments:

```
in_place_jq -- "-file.json" .
```

# Make targets

* `make` - builds *bin/in_place_jq*
* `make tidy` - runs *clang-tidy*
* `make format` - runs *clang-format*

[jq]: https://stedolan.github.io/jq/

