
CFLAGS = -D_GNU_SOURCE

all: bin/in_place_jq
.PHONY: all

bin/in_place_jq: main.c
	@echo CC main.c
	@mkdir -p bin
	@gcc -o bin/in_place_jq main.c $(CFLAGS)

tidy:
	@clang-tidy --fix -checks=-\*,bugprone-\* main.c -- $(CFLAGS)
.PHONY: tidy

format:
	@clang-format -i main.c
.PHONY: fmt

